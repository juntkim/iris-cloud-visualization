/*
 * define.h
 *
 *  Created on: Nov 29, 2018
 *      Author: june
 */

#ifndef DEFINE_H_
#define DEFINE_H_

#define FILEDUMP 0
#define LOG_FRAMEINFO 0
#define MAX_NUM_CHANNEL 10
#define MAX_NUM_VIDEOTRACK 5
#define MAX_NUM_AUDIOTRACK 5
#define MAX_NUM_TRACK 5
#define MAX_NUM_VISUALIZATION 8
#define BUFFER_SIZE 60
#define MAX_NUM_SAMPLEPERFRAME 4096
#define MAX_NUM_WAVEFORMSTEP 5
#define PI 3.141592654
#define MAX_NUM_PHASEMETERSTEP 3


#endif /* DEFINE_H_ */
