/*
 * Copyright (c) 2012 Stefano Sabatini
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
/**
 * @file
 * Demuxing and decoding example.
 *
 * Show how to use the libavformat and libavcodec API to demux and
 * decode audio and video data.
 * @example demuxing_decoding.c
 */
#include <libavutil/imgutils.h>
#include <libavutil/samplefmt.h>
#include <libavutil/timestamp.h>
#include <libavformat/avformat.h>
#include <libavformat/avio.h>
#include <libswresample/swresample.h>
#include <math.h>
#include <pthread.h>

#include "audio.h"
#include "define.h"
#include "global.h"

void itoa(int n, char* output)
{
    int len = n==0 ? 1 : floor(log10l(labs(n)))+1;
    if (n<0) len++; // room for negative sign '-'

    //char    buf[200]; // +1 for null
    snprintf(output, len+1, "%d", n);
   // printf("itoa==========%s\n", output);
    //return  output;
}

char* makeArrayFilename(char* dst, const char* name, int arrayNum, int isJson) {
	int k=0;
//	char* filename = (char*)malloc(200*sizeof(char));
	char filename[200] = {}; //= (char*)calloc(200, sizeof(char));
	int dot_pos = -1;
	char num[200] = {};

	while (*(name+k) != 0){
		if(*(name+k) == '.'){
			dot_pos = k;
	    }
	    k++;
	}
	if(dot_pos == -1){
		strcpy(filename, name);
	}else{
		memcpy(filename, name, dot_pos);
	}
	itoa(arrayNum, &num[0]);
//	num = (char)arrayNum;
	strcat(filename, "_track");
	strcat(filename, num);
	if(isJson){
		strcat(filename, ".json");
	}
	//printf("makeArrayFilename==========%s\n", filename);
	memcpy(dst, filename, strlen(filename)+1);
//	dst = result;
	return dst;
}

char* makeFilenameFromBaseName(char* dst, const char* name, int trackNum, int index, int sub_index, char* filepath) {
	char num[10] = {};
	//char* filename = (char*)malloc(200*sizeof(char));
	//char* filename = (char*)calloc(200,sizeof(char));
	char filename[200] = {};
	char Idx[10] = {};
	//dst = NULL;

	itoa(trackNum, &num[0]);
	//printf("%s\n",num);
	itoa(sub_index, &Idx[0]);
	//printf("%s\n",Idx);
	//memcpy(filename, name, strlen(name));
	memcpy(filename, filepath, strlen(filepath));
//	printf("%s\n",filename);
	strcat(filename, name);
//	printf("%s\n",filename);
	switch(index){
		case 0:
			strcat(filename, "_audiolevelmeters");
			break;
		case 1:
			strcat(filename, "_audiowaveform");
			strcat(filename, Idx);
			break;
		case 2:
			strcat(filename, "_audiophasemeter");
			strcat(filename, Idx);
			break;
		case 3:
			strcat(filename, "_loudnessmonitor");
			break;
		case 4:
			strcat(filename, "_vectorscope");
			break;
		case 5:
			strcat(filename, "_spectrascope");
			break;
		case 6:
			strcat(filename, "_videowaveform");
			break;
		case 7:
			//reserved
			break;
	}


	strcat(filename, "_track");
	strcat(filename, num);
	strcat(filename, ".json");
	//printf("1======================%s\n", filename);

	memcpy(dst, filename, strlen(filename)+1);
	//printf("2======================%s\n", dst);
	return dst;
}

void initSharingContext(AVCodecContext* ctx[]){
	for(int i=0; i<audiometer.numOfAudioTrack; i++){
		audiometer.audio_dec_ctx[i] = (AVCodecContext*)malloc(sizeof(AVCodecContext));
	}
	for(int i=0; i<audiometer.numOfVideoTrack; i++){
		audiometer.video_dec_ctx[i] = (AVCodecContext*)malloc(sizeof(AVCodecContext));
	}
	for(int j=0; j<audiometer.numOfAudioTrack; j++){
		for(int i=0; i<BUFFER_SIZE; i++){
			audiometer.frame[j][i] = (AVFrame*) malloc(sizeof(AVFrame));
			audiometer.input_samples[j][i] = (uint8_t*)malloc(MAX_NUM_CHANNEL * MAX_NUM_SAMPLEPERFRAME * av_get_bytes_per_sample(AV_SAMPLE_FMT_S32));
			audiometer.processDone[j][i] = 0;
			audiometer.saveDone[j][i] = 0;
		}
	}

	for(int i=0; i<audiometer.numOfAudioTrack; i++){
		audiometer.endofStream[i] = INT_MAX;
		memcpy(audiometer.audio_dec_ctx[i], ctx[i], sizeof(AVCodecContext));
	}
};

void clearSharingContext(){
	for(int j=0; j<audiometer.numOfAudioTrack; j++){
			for(int i=0; i<BUFFER_SIZE; i++){
				free(audiometer.frame[j][i]);
				free(audiometer.input_samples[j][i]);
				audiometer.frame[j][i] = NULL;
				audiometer.input_samples[j][i] = NULL;
			}
		}
	for(int i=0; i<audiometer.numOfAudioTrack; i++){
		free(audiometer.audio_dec_ctx[i]);
		audiometer.audio_dec_ctx[i] = NULL;
	}
	for(int i=0; i<audiometer.numOfVideoTrack; i++){
		free(audiometer.video_dec_ctx[i]);
		audiometer.video_dec_ctx[i] = NULL;
	}
	free(filename_edited);
	filename_edited = NULL;
}

int ConvertAudio(SwrContext *pSwrCtx,
		const AVFrame* pFrame,
		const uint8_t **pdata,
		int InSampleRate,
		int OutSampleRate,
		enum AVSampleFormat InSampleFormat,
		enum AVSampleFormat OutSampleFormat,
		uint8_t **pOutput){
	int dst_nb_samples = av_rescale_rnd(pFrame->nb_samples, InSampleRate, OutSampleRate, AV_ROUND_UP);
	int nRet = av_samples_alloc(pOutput, NULL, pFrame->channels, dst_nb_samples, OutSampleFormat, 0);
	nRet = swr_convert(pSwrCtx, pOutput, dst_nb_samples, pdata, pFrame->nb_samples);
			return nRet;
}

int bufferFullCheck(int streamIndex,int frameCnt){
	int bufferIdx = (frameCnt-1)%BUFFER_SIZE;

	if(frameCnt == 0){
		return 0; //initially dont wait
	}


	if(frameCnt/BUFFER_SIZE >=1){ //only wait after 1 cycle of the buffer array which means all buffers are full
		if(audiometer.processDone[streamIndex][bufferIdx] < 1){
			return 1; //wait until previous buffer is done
		}else{ //prcessDone==1 means finished all calculation. it's okay to flush it.
			audiometer.processDone[streamIndex][bufferIdx] = 0;
			return 0; //don't wait
		}
	}else{
		return 0; //don't wait
	}
}

void bufferCopy(int frameCnt, int streamIndex,uint8_t *input_samples, AVFrame* frame){
	int bufferIdx = frameCnt%BUFFER_SIZE;
	memcpy(audiometer.frame[streamIndex][bufferIdx], frame, sizeof(AVFrame));
	memcpy(audiometer.input_samples[streamIndex][bufferIdx], input_samples,
			audiometer.frame[streamIndex][bufferIdx]->channels *audiometer.frame[streamIndex][bufferIdx]->nb_samples
			* av_get_bytes_per_sample(AV_SAMPLE_FMT_S32));
	audiometer.saveDone[streamIndex][bufferIdx] = 1;
	//printf("streamIndex%d bufferIdx%d done%d\n",streamIndex, bufferIdx, audiometer.saveDone[streamIndex][bufferIdx] );


}

int decode_packet(AVFrame* frame, AVPacket pkt, int *got_frame, int cached)
{
    int ret = 0;
    int decoded = pkt.size;
//    signed short y = 0;

    int ch_num = 0;
    *got_frame = 0;
    int stream_index = -1; // indicate audio/video track index
    int stream_type = -1; // -1: none 0:video 1:audio
    SwrContext * resample_context = NULL;

    for(int i=0; i<audiometer.numOfVideoTrack;i++){
    	if(pkt.stream_index == video_stream_idx[i]){
    		stream_type = 0;
    		stream_index = i;
    	}
    }

    for(int i=0; i<audiometer.numOfAudioTrack;i++){
    	if(pkt.stream_index == audio_stream_idx[i]){
    		stream_type = 1;
    	    stream_index = i;
    	}
    }

    if (stream_type == 0) {
        /* decode video frame */

        ret = avcodec_decode_video2(video_dec_ctx[stream_index], frame, got_frame, &pkt);
       // printf("Video frame%d pkt.duration%d\n",videoFrameCount[stream_index], pkt.duration);
        if (ret < 0) {
            printf("Error decoding video frame (%s)\n", av_err2str(ret));
            return ret;
        }
        if (*got_frame) {
            if (frame->width != width || frame->height != height ||
                frame->format != pix_fmt) {
                /* To handle this change, one could call av_image_alloc again and
                 * decode the following frames into another rawvideo file. */
                printf("Error: Width, height and pixel format have to be "
                        "constant in a rawvideo file, but the width, height or "
                        "pixel format of the input video changed:\n"
                        "old: width = %d, height = %d, format = %s\n"
                        "new: width = %d, height = %d, format = %s\n",
                        width, height, av_get_pix_fmt_name(pix_fmt),
                        frame->width, frame->height,
                        av_get_pix_fmt_name(frame->format));
                return -1;
            }

#if LOG_FRAMEINFO
            float timestamp = frame->coded_picture_number/(float)video_dec_ctx[stream_index]->framerate.num;
            printf("video_frame n:%d fps:%.4f coded_n:%d\n", video_frame_count++, timestamp, frame->coded_picture_number);
#endif
            /* copy decoded frame to destination buffer:
             * this is required since rawvideo expects non aligned data */
            av_image_copy(video_dst_data[stream_index], video_dst_linesize[stream_index],
                          (const uint8_t **)(frame->data), frame->linesize,
                          pix_fmt, width, height);
            videoFrameCount[stream_index]++;
            /* write to rawvideo file */
#if FILEDUMP
            fwrite(video_dst_data[stream_index][0], 1, video_dst_bufsize[stream_index],
            		video_dst_file[stream_index]);
#endif
        }
    } else if (stream_type == 1){

        /* decode audio frame */

        ret = avcodec_decode_audio4(audio_dec_ctx[stream_index], frame, got_frame, &pkt);
    //    printf("Audio frame%d pkt.duration%d\n",audioFrameCount[stream_index], pkt.duration);
        ch_num = frame->channels;
        int channel_layout = AV_CH_FRONT_LEFT;
        for(int i=0; i<ch_num; i++){
        	channel_layout |= (1LL << i);
        }
        if (ret < 0) {
            printf("Error decoding audio frame (%s)\n", av_err2str(ret));
            return ret;
        }



        if(resample_context){
        	swr_free(&resample_context);
        	resample_context = NULL;
        }
        resample_context = swr_alloc();
        swr_alloc_set_opts(resample_context, channel_layout, AV_SAMPLE_FMT_S32,
        		audio_dec_ctx[stream_index]->sample_rate, channel_layout,
				audio_dec_ctx[stream_index]->sample_fmt,
				audio_dec_ctx[stream_index]->sample_rate, 0, 0);
        swr_init(resample_context);

        uint8_t *converted_input_samples = NULL;
        const uint8_t** pdata = (const uint8_t**)frame->extended_data;

        ConvertAudio(resample_context, frame, pdata,
        		audio_dec_ctx[stream_index]->sample_rate,
				audio_dec_ctx[stream_index]->sample_rate,
        		frame->format, AV_SAMPLE_FMT_S32, &converted_input_samples);

        /* Some audio decoders decode only part of the packet, and have to be
         * called again with the remainder of the packet data.
         * Sample: fate-suite/lossless-audio/luckynight-partial.shn
         * Also, some decoders might over-read the packet. */
        decoded = FFMIN(ret, pkt.size);
        if (*got_frame) {
        	while(bufferFullCheck(stream_index, audioFrameCount[stream_index])){
        		usleep(5);
        	}
        	bufferCopy(audioFrameCount[stream_index], stream_index, converted_input_samples, frame);
        	audioFrameCount[stream_index]++;
        	av_freep(&converted_input_samples);
        	converted_input_samples = NULL;
        }
    }else{
    	return -1;
    }
    /* If we use frame reference counting, we own the data and need
     * to de-reference it when we don't use it anymore */
    if (*got_frame){
        av_frame_unref(frame);
    }
    if(resample_context){
    	swr_free(&resample_context);

        resample_context = NULL;
    }

    return decoded;
}



int open_codec_context(int *stream_idx,
                              AVCodecContext **dec_ctx, AVFormatContext *fmt_ctx, enum AVMediaType type)
{
    int ret = -1;
	int stream_index;
    AVStream *st;
    AVCodec *dec = NULL;
    AVDictionary *opts = NULL;
    AVCodecContext *dec_context;
    int track_index[MAX_NUM_AUDIOTRACK>MAX_NUM_VIDEOTRACK?MAX_NUM_AUDIOTRACK:MAX_NUM_VIDEOTRACK] = {0};
    audiometer.numOfAudioTrack = 0;

    if(type == AVMEDIA_TYPE_VIDEO){
    	for(int i=0; i< fmt_ctx->nb_streams; i++){
    		if( i == av_find_best_stream(fmt_ctx, type, i, -1, NULL, 0)){
    			track_index[audiometer.numOfVideoTrack] = i;
    	    	audiometer.numOfVideoTrack++;
    	    	ret = i;
    	   	}
    	}
    }
    else if(type == AVMEDIA_TYPE_AUDIO){
    	for(int i=0; i< fmt_ctx->nb_streams; i++){
    		if( i == av_find_best_stream(fmt_ctx, type, i, -1, NULL, 0)){
    			track_index[audiometer.numOfAudioTrack] = i;
    			audiometer.numOfAudioTrack++;
    			ret = i;
    		}
    	}
    }
    if (ret < 0) {
        printf("Could not find %s stream in input file '%s'\n",
                av_get_media_type_string(type), src_filename);
        return ret;
    } else {
    	if(type == AVMEDIA_TYPE_VIDEO){
    		for(int i=0; i<audiometer.numOfVideoTrack; i++){
    			stream_index = track_index[i];
//			stream_index = ret;
			st = fmt_ctx->streams[stream_index];
			/* find decoder for the stream */
			dec = avcodec_find_decoder(st->codecpar->codec_id);
			if (!dec) {
				printf("Failed to find %s codec\n",
						av_get_media_type_string(type));
				return AVERROR(EINVAL);
			}
			/* Allocate a codec context for the decoder */
//			*dec_ctx = avcodec_alloc_context3(dec);
			dec_context = avcodec_alloc_context3(dec);
			if (!dec_context) {
				printf("Failed to allocate the %s codec context\n",
						av_get_media_type_string(type));
				return AVERROR(ENOMEM);
			}
			/* Copy codec parameters from input stream to output codec context */
			if ((ret = avcodec_parameters_to_context(dec_context, st->codecpar)) < 0) {
				printf("Failed to copy %s codec parameters to decoder context\n",
						av_get_media_type_string(type));
				return ret;
			}
			/* Init the decoders, with or without reference counting */
			av_dict_set(&opts, "refcounted_frames", refcount ? "1" : "0", 0);
			if ((ret = avcodec_open2(dec_context, dec, &opts)) < 0) {
				printf("Failed to open %s codec\n",
						av_get_media_type_string(type));
				return ret;
			}
			dec_ctx[i] = dec_context;
			stream_idx[i] = stream_index;
    		}
    	}else if(type == AVMEDIA_TYPE_AUDIO){
    		for(int i=0; i<audiometer.numOfAudioTrack; i++){
    			stream_index = track_index[i];

				st = fmt_ctx->streams[stream_index];
				if(st->nb_frames>0)
					nbframes = st->nb_frames;
				else
					nbframes = 0;

				//printf("%d\n", nbframes);
				/* find decoder for the stream */
				dec = avcodec_find_decoder(st->codecpar->codec_id);
				if (!dec) {
					printf("Failed to find %s codec\n",
					av_get_media_type_string(type));
					return AVERROR(EINVAL);
				}
				/* Allocate a codec context for the decoder */

				dec_context = avcodec_alloc_context3(dec);
				if (!dec_context) {
					printf("Failed to allocate the %s codec context\n",
							av_get_media_type_string(type));
					return AVERROR(ENOMEM);
				}
				/* Copy codec parameters from input stream to output codec context */
				if ((ret = avcodec_parameters_to_context(dec_context, st->codecpar)) < 0) {
					printf("Failed to copy %s codec parameters to decoder context\n",
							av_get_media_type_string(type));
					return ret;
				}
				/* Init the decoders, with or without reference counting */
				av_dict_set(&opts, "refcounted_frames", refcount ? "1" : "0", 0);
				if ((ret = avcodec_open2(dec_context, dec, &opts)) < 0) {
					printf("Failed to open %s codec\n",
							av_get_media_type_string(type));
				return ret;
				}
				dec_ctx[i] = dec_context;
				stream_idx[i] = stream_index;
    		}
    	}
    }
    return 0;
}
int get_format_from_sample_fmt(const char **fmt,
                                      enum AVSampleFormat sample_fmt)
{
    int i;
    struct sample_fmt_entry {
        enum AVSampleFormat sample_fmt; const char *fmt_be, *fmt_le;
    } sample_fmt_entries[] = {
        { AV_SAMPLE_FMT_U8,  "u8",    "u8"    },
        { AV_SAMPLE_FMT_S16, "s16be", "s16le" },
        { AV_SAMPLE_FMT_S32, "s32be", "s32le" },
        { AV_SAMPLE_FMT_FLT, "f32be", "f32le" },
        { AV_SAMPLE_FMT_DBL, "f64be", "f64le" },
    };
    *fmt = NULL;
    for (i = 0; i < FF_ARRAY_ELEMS(sample_fmt_entries); i++) {
        struct sample_fmt_entry *entry = &sample_fmt_entries[i];
        if (sample_fmt == entry->sample_fmt) {
            *fmt = AV_NE(entry->fmt_be, entry->fmt_le);
            return 0;
        }
    }
    printf(
            "sample format %s is not supported as output format\n",
            av_get_sample_fmt_name(sample_fmt));
    return -1;
}
int main(int argc, char **argv)
{
    int ret = 0, got_frame;
    AVFrame *frame = NULL;
    AVPacket pkt;
    pthread_t tid[MAX_NUM_AUDIOTRACK];
    FILE* log_file;
    char* log_filename[100];
   // int id[MAX_NUM_AUDIOTRACK];
    int idx;
    int bytes = 0;

    refcount = 0;
    video_frame_count = 0;

    for(int i=0; i< MAX_NUM_AUDIOTRACK; i++){
    	audioFrameCount[i] = 0;
    	videoFrameCount[i] = 0;
    }


//    if (argc == 5 && !strcmp(argv[1], "-refcount")) {
//        refcount = 1;
//        argv++;
//    }
    src_filename = argv[1];
  //  video_dst_filename = argv[2];
  //  audio_dst_filename = argv[3];
 //   progress_outfilename = "progress.txt";
//    audiolevelmeters_filename = argv[4];
    feature_index = atoi(argv[2]);
    base_filename = argv[3];
    char*filepath;
    filepath = argv[4];
    memcpy(log_filename, filepath, strlen(filepath));
    //	printf("%s\n",filename);
    strcat(log_filename, "log.txt");
    log_file = fopen(log_filename, "wb");

    if (argc != 5) {
            fprintf(log_file, "Error: Invalid argument\n"
            		"usage: %s %s %s %s %s\n"
            		, argv[0], argv[1], argv[2], argv[3], argv[4]);
            ret = 201;
            printf("201\n");
            exit(201);
        }

    /* open input file, and allocate format context */
    if (avformat_open_input(&fmt_ctx, src_filename, NULL, NULL) < 0) {
        fprintf(log_file, "Error: Cannot find source file %s\n", src_filename);
        ret = 202;
        printf("202\n");
        exit(202);
    }
    /* retrieve stream information */
    if (avformat_find_stream_info(fmt_ctx, NULL) < 0) {
        fprintf(log_file, "Error: Could not find stream information\n");
        ret = 203;
        printf("203\n");
        exit(203);
    }
    if (open_codec_context(&video_stream_idx[0], &video_dec_ctx[0], fmt_ctx, AVMEDIA_TYPE_VIDEO) >= 0) {
    	for(int i=0; i< audiometer.numOfVideoTrack; i++){
    		video_stream[i] = fmt_ctx->streams[video_stream_idx[i]];

#if FILEDUMP
    		if(audiometer.numOfVideoTrack == 1){
    			video_dst_file[i] = fopen(video_dst_filename, "wb");
    			video_dst_filename2[i] = (char *)video_dst_filename;
    		}else{

    			video_dst_filename2[i] = makeArrayFilename(video_dst_filename2[i],video_dst_filename, i, 0);
				video_dst_file[i] = fopen(video_dst_filename2[i], "wb");
    		}
			if (!video_dst_file[i]) {
				printf("Could not open destination file %s\n", video_dst_filename2[i]);
				ret = 1;
				goto end;
			}
#endif
    	}
        /* allocate image where the decoded image will be put */
        for(int i=0; i< audiometer.numOfVideoTrack; i++){
        	width = video_dec_ctx[i]->width;
        	height = video_dec_ctx[i]->height;
        	pix_fmt = video_dec_ctx[i]->pix_fmt;
			ret = av_image_alloc(video_dst_data[i], video_dst_linesize[i],
								 width, height, pix_fmt, 1);
			if (ret < 0) {
				fprintf(log_file, "Error: Could not allocate a video buffer\n");
				ret = 204;
				goto end;
			}
			video_dst_bufsize[i] = ret;
        }


    }
    if (open_codec_context(&audio_stream_idx[0], &audio_dec_ctx[0], fmt_ctx, AVMEDIA_TYPE_AUDIO) >= 0) {

    	initSharingContext(audio_dec_ctx);
    	for(int i=0; i< audiometer.numOfAudioTrack; i++){
    		audio_stream[i] = fmt_ctx->streams[audio_stream_idx[i]];

    		idx = feature_index;

    	//	progress_outfile = fopen(progress_outfilename, "wb");
			for(int j=0; j<MAX_NUM_VISUALIZATION; j++){
				if(idx && 0x01){
					if(j==1){ //if(AUDIO_WAVEFORM)
						for(int k=0; k<MAX_NUM_WAVEFORMSTEP; k++){
							audiowaveform_filename[k][i] = (char*)calloc(300, sizeof(char));
							audiowaveform_filename[k][i] = makeFilenameFromBaseName(audiowaveform_filename[k][i] ,base_filename, i, j, k, filepath );
						//	strcat(audiowaveform_filename[k][i], filepath);
						//	printf("%s\n", audiowaveform_filename[k][i]);
							audio_waveform[k][i] = fopen(audiowaveform_filename[k][i], "wb");
						}
					}else if(j==2){ // if(AUDIO_PHASEMETERS)
						for(int k=0; k<MAX_NUM_PHASEMETERSTEP; k++){
							audiophase_filename[k][i] = (char*)calloc(300, sizeof(char));
							audiophase_filename[k][i] = makeFilenameFromBaseName(audiophase_filename[k][i] ,base_filename, i, j, k, filepath);
							//strcat(audiophase_filename[k][i], filepath);
						//printf("%s\n", audiophase_filename[k][i]);
							audiophase_file[k][i] = fopen(audiophase_filename[k][i], "wb");
						}
					}
					else{
						output_filename[j][i] = (char*)calloc(300, sizeof(char));
						output_filename[j][i] = makeFilenameFromBaseName(output_filename[j][i] ,base_filename, i, j, 0, filepath );
					//	strcat(output_filename[j][i], filepath);
						//printf("%s \n", output_filename[j][i]);
						output_file[j][i] = fopen(output_filename[j][i], "wb");
					}
				}
				idx = idx >> 1;
			}

			if (!output_filename[0][i]) {
				fprintf(log_file, "Error: Could not open AudioLevelMeters file %s\n", output_filename[0][i]);
				ret = 205;
				goto end;
			}
			//id[i] = i;
			tcontext[i].audioTrackIdx = i;
			tcontext[i].featureIdx = feature_index;
			pthread_create(&tid[i], NULL, AudioLevelMetersFunc,&tcontext[i]);


#if FILEDUMP
			if(audiometer.numOfAudioTrack == 1){
				audio_dst_file[i] = fopen(audio_dst_filename, "wb");
				audio_dst_filename2[i] = (char *)audio_dst_filename;
			}else{
				audio_dst_filename2[i] = makeArrayFilename(audio_dst_filename2[i], audio_dst_filename, i, 0);
				audio_dst_file[i] = fopen(audio_dst_filename2[i], "wb");
			}
			if (!audio_dst_file[i]) {
				printf("Could not open destination file %s\n", audio_dst_filename2[i]);
				ret = 1;
				goto end;
			}
#endif
    	}
    }
    /* dump input information to stderr */
//    av_dump_format(fmt_ctx, 0, src_filename, 0);

  //  av_dump_format(fmt_ctx, audio_stream_idx[0], src_filename, 0);

    for(int i=0; i< audiometer.numOfAudioTrack; i++){
		if (!audio_stream[i] && !video_stream[i]) {
			fprintf(log_file, "Error: Could not find audio or video stream in the input, aborting\n");
			printf("206\n");
			ret = 206;
			goto end;
		}
    }
    frame = av_frame_alloc();
    if (!frame) {
    	fprintf(log_file, "Error: Could not allocate frame buffer\n");
        ret = AVERROR(ENOMEM);
        ret = 207;
        printf("207\n");
        goto end;
    }
    /* initialize packet, set data to NULL, let the demuxer fill it */
    av_init_packet(&pkt);
    pkt.data = NULL;
    pkt.size = 0;

/*
    for(int i=0; i< audiometer.numOfAudioTrack; i++){
//    	printf("There %s %d tracks in Audio.\n", (audiometer.numOfAudioTrack==1)?"is":"are", audiometer.numOfAudioTrack);
//		printf("Making Audio Level Meters from file '%s' into '%s'\n", src_filename, output_filename[0][i]);
		if (audio_stream[i])
			printf("Demuxing audio from file '%s' into '%s'\n", src_filename, audio_dst_filename2[i]);
    }
    for(int i=0; i< audiometer.numOfVideoTrack; i++){
		if (video_stream[i])
			printf("Demuxing video from file '%s' into '%s'\n", src_filename, video_dst_filename2[i]);

    }
    */

    /* read frames from the file */
    while (av_read_frame(fmt_ctx, &pkt) >= 0) {
        AVPacket orig_pkt = pkt;
        do {

            bytes = decode_packet(frame, pkt, &got_frame, 0);
            if (bytes < 0)
                break;
            pkt.data += bytes;
            pkt.size -= bytes;
        } while (pkt.size > 0);
        av_packet_unref(&orig_pkt);
    }
    /* flush cached frames */
    pkt.data = NULL;
    pkt.size = 0;



    do {
    	//printf("last packet?\n");
        decode_packet(frame, pkt, &got_frame, 1);
    } while (got_frame);

    for(int i=0; i<audiometer.numOfAudioTrack; i++){
    	audiometer.endofStream[i] = audioFrameCount[i];
    }

  //  printf("Demuxing succeeded EOF%d.\n", audiometer.endOfStream);

    if (video_stream[0]) {
      //  printf("Play the output video file with the command:\n"
        //       "ffplay -f rawvideo -pix_fmt %s -video_size %dx%d %s\n",
         //      av_get_pix_fmt_name(pix_fmt), width, height,
        //       video_dst_filename);
    }
    for(int i=0; i<audiometer.numOfAudioTrack; i++){
		if (audio_stream[i]) {
			pthread_join(tid[i], NULL);


		//	for(int i=0; i< audiometer.numOfAudioTrack; i++){

		//	}
		//	pthread_detach(tid[i]);
			enum AVSampleFormat sfmt = AV_SAMPLE_FMT_S32; //audio_dec_ctx->sample_fmt;
		//	int n_channels = audio_dec_ctx[i]->channels;
			const char *fmt;
			if (av_sample_fmt_is_planar(sfmt)) {
				const char *packed = av_get_sample_fmt_name(sfmt);
				//printf("Warning: the sample format the decoder produced is planar "
				//	   "(%s).\n",
				//	   packed ? packed : "?");
				sfmt = av_get_packed_sample_fmt(sfmt);
			}
			if ((ret = get_format_from_sample_fmt(&fmt, sfmt)) < 0){
				ret = 208;
				goto end;
			}
			//printf("Play the output audio file, track number %d, with the command:\n"
		//		   "ffplay -f %s -ac %d -ar %d %s\n",
		//		   i, fmt, n_channels, audio_dec_ctx[i]->sample_rate,
		//		   audio_dst_filename);
		}
    }
end:

	for(int i=0; i<audiometer.numOfAudioTrack; i++){
		avcodec_free_context(&video_dec_ctx[i]);
		avcodec_free_context(&audio_dec_ctx[i]);
	}
    clearSharingContext();
    avformat_close_input(&fmt_ctx);

#if FILEDUMP
    for(int i=0; i<audiometer.numOfVideoTrack; i++){
    	if (video_dst_file[i])
    		fclose(video_dst_file[i]);
    }
    for(int i=0; i<audiometer.numOfAudioTrack; i++){
		if (audio_dst_file[i])
			fclose(audio_dst_file[i]);
    }
#endif
    idx = feature_index;
    for(int i=0; i<audiometer.numOfAudioTrack; i++){

//    	if(audiolevelmeters_file[i]){
//    		fclose(audiolevelmeters_file[i]);
//    	}

    	for(int j=0; j<MAX_NUM_VISUALIZATION; j++){
//    		if(j != 1){
//    			free(output_filename[j][i]);
//    		}
    		idx = feature_index;
    		if(idx && 0x01){
				if(j == 0){
					if(output_file[j][i]){

						fclose(output_file[j][i]);
					}
					if(output_filename[j][i]){
						free(output_filename[j][i]);
						output_filename[j][i] = NULL;
					}
				}else if(j == 1){
					for(int j=0; j<MAX_NUM_WAVEFORMSTEP; j++){
						if(audio_waveform[j][i])
							fclose(audio_waveform[j][i]);
						if(audiowaveform_filename[j][i]){
							free(audiowaveform_filename[j][i]);
							audiowaveform_filename[j][i] = NULL;
						}
					}
				}else if(j==2){
					for(int j=0; j<MAX_NUM_PHASEMETERSTEP; j++){
						if(audiophase_file[j][i])
							fclose(audiophase_file[j][i]);
						if(audiophase_filename[j][i]){
							free(audiophase_filename[j][i]);
							audiophase_filename[j][i] = NULL;
						}
					}
				}
    		}

			idx = idx >> 1;
    	}


//    	free(video_dst_filename2[i]);
    }


  //  fclose(progress_outfile);

    av_freep(&frame);
    frame = NULL;
    //av_frame_free(&frame);

    for(int i=0; i<audiometer.numOfVideoTrack; i++){
    	av_free(video_dst_data[i][0]);
    	video_dst_data[i][0] = NULL;
    }

    if(ret == 0){
    	fprintf(log_file, "Successful\n");
    	printf("Successful\n");
    }
    else{
    	fprintf(log_file, "Failure %d\n", ret);
    	printf("Failure %d\n", ret);
    }
    fclose(log_file);
    return ret;
}
