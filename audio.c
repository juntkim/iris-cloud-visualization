/*
 * audiolevelmeters.c
 *
 *  Created on: Nov 22, 2018
 *      Author: june
 */

#include "audio.h"

#include <libavformat/avformat.h>
#include <libavformat/avio.h>
#include <libavutil/imgutils.h>
#include <libavutil/samplefmt.h>
#include <libavutil/timestamp.h>
#include "global.h"

void swapInt(int *a, int* b){
	int t = *a;
	*a = *b;
	*b = t;
}

void swapChar(char *a, char* b){
	char t = *a;
	*a = *b;
	*b = t;
}
int partition (int sqsum[], char x[], char y[], int low, int high){
	int pivot = sqsum[high];
	int i = (low-1);

	for(int j=low; j<=high-1; j++){
		if(sqsum[j]<=pivot){
			i++;
			swapInt(&sqsum[i], &sqsum[j]);
			swapChar(&x[i], &x[j]);
			swapChar(&y[i], &y[j]);
		}
	}
	swapInt(&sqsum[i+1], &sqsum[high]);
	swapChar(&x[i+1], &x[high]);
	swapChar(&y[i+1], &y[high]);

	return(i+1);
}

void quickSort(int sqsum[], char x[], char y[], int low, int high){

	if(low < high){
		int pi = partition(sqsum,x,y, low, high);

		quickSort(sqsum, x, y, low, pi-1);
		quickSort(sqsum, x, y, pi+1, high);
	}
}

void printArray(char arr[], int size){
	int i;
	for(i=0; i<size; i++)
		printf("%d ", arr[i]);
	printf(" n %d\n", size);
}

int removeDuplicates(char arr[], char arr2[], int arr3[], int size){
	//int size = sizeof(arr)/sizeof(char);
	int i,j,k;
	//printf("================ %d %d size%d\n", arr[0], arr2[0], size);
	for(i=0; i< size; i++){
		for(j=i+1; j<size;){
			if(arr[j] == arr[i] && arr2[j] == arr2[i]){
				for(k=j; k<size; k++){
					arr[k] = arr[k+1];
					arr2[k] = arr2[k+1];
					arr3[k] = arr3[k+1];
				}
				size--;
			} else{
				j++;
			}
		}
	}

	return size;
}

void RatateAudioPhase(float angle, char* x, char* y){
	float h = sqrt((*y)*(*y) + (*x)*(*x));
	float alpha = atan((float)abs(*y) / (float)abs(*x));
//	printf("1x%d y%d h%f alpha%f\n", *x,*y, h, alpha);
	if(*x<0 && *y>=0)
		alpha = (float)PI - alpha;
	else if(*x<0 && *y<0)
		alpha = alpha + (float)PI;
	else if(*x>=0 && *y<0)
		alpha = (float)(2*PI) - alpha;
//	printf("2x%d y%d h%f alpha%f\n", *x,*y, h, alpha);
	alpha += (float)((angle/180.0)*PI);
//	printf("3x%d y%d h%f alpha%f\n", *x,*y, h, alpha);
	*y = (sin(alpha)*h)+0.5;
	*x = (cos(alpha)*h)+0.5;
//	printf("4x%d y%d h%f alpha%f\n", *x,*y, h, alpha);
}

void writeJsonBegin(FILE* file){
	fprintf(file, "{\n");
	fprintf(file, "   \"frame\" : [\n");
}

void writeJsonEnd(FILE* file){
	fprintf(file, "   ]\n");
	fprintf(file, "}");
}

void writeJsonBetweenFrame(FILE* file){
	fprintf(file, ",\n");
}

void writeJsonFrameAudioLevelMeters(FILE* file, int chnum, AVFrame* frame, int levelDbInt[], int trackIdx){
//	printf("========================inframe%d trackIdx%d\n", audio_frame_count[trackIdx], trackIdx);
	fprintf(file, "      {\n");
	fprintf(file, "         \"index\" : %d,\n", audio_frame_count[trackIdx]);
	fprintf(file, "         \"pts\" : %lf,\n", (double)(audio_frame_count[trackIdx]
			*av_get_audio_frame_duration(audio_dec_ctx[trackIdx], frame->pkt_size) /
			(double)audio_dec_ctx[trackIdx]->time_base.den));
	fprintf(file, "         \"level\" : [");
	for(int i=0; i<chnum; i++){
		fprintf(file, "%d ", levelDbInt[i]);
		if(i<chnum-1){
			fprintf(file,",");
		}
	}
	fprintf(file,"]\n");
	fprintf(file, "      }");
}
void writeJsonInfoAudioWaveform(FILE* file, int chnum, int trackIdx, int* min, int* max, double totalTime, AVFrame* frame){
	fprintf(file, "   ],\n");
	fprintf(file, "   \"info\" : {\n");
	fprintf(file, "      \"channels\" : %d,\n", chnum);
	double x = av_get_audio_frame_duration(audio_dec_ctx[trackIdx], frame->pkt_size)
				/ (double)audio_dec_ctx[trackIdx]->time_base.den;
	totalTime = (double)(audiometer.endofStream[trackIdx]*x);
	fprintf(file, "      \"duration\" : %lf,\n", totalTime);
	fprintf(file, "      \"min\" : [");
	for(int i=0; i<chnum; i++){
		fprintf(file, "%d", *(min+i));
		if(i<chnum-1){
			fprintf(file,",");
		}
	}
	fprintf(file,"],\n");
	fprintf(file, "      \"max\" : [");
	for(int i=0; i<chnum; i++){
		fprintf(file, "%d", *(max+i));
		if(i<chnum-1){
			fprintf(file,",");
		}
	}
	fprintf(file,"]\n");
	fprintf(file,"   }\n}");

	//fprintf(file, "         \"pts\" : %lf,\n", (double)(audio_frame_count[trackIdx]
}

void writeJsonFrameAudioWaveform(FILE* file, int chnum, AVFrame* frame, int* min, int* max, int trackIdx,
		int unitIdx, int searchRange, double* totalTime){
	double x = av_get_audio_frame_duration(audio_dec_ctx[trackIdx], frame->pkt_size)
			/ (double)audio_dec_ctx[trackIdx]->time_base.den;
//	double y = x/totalUnits * unitIdx;
//	double y = x * unitIdx;
//	printf("========================inframe%d trackIdx%d unitIdx%d\n", audio_frame_count[trackIdx], trackIdx, unitIdx);
	//printf("%d timebase %d\n", av_get_audio_frame_duration(audio_dec_ctx[trackIdx], frame->pkt_size), audio_dec_ctx[trackIdx]->time_base.den);
	//printf("%d\n", av_rescale_q(1, (AVRational){1, 1000000}, audio_dec_ctx[trackIdx]->time_base));
	if(audio_frame_count[trackIdx]+1 != searchRange){
		fprintf(file, ",\n");
	}
	fprintf(file, "      {\n");
	fprintf(file, "         \"index\" : %d,\n", audio_frame_count[trackIdx]);
	//*totalTime = (double)(audio_frame_count[trackIdx]*x);
	//printf("track%d enfFrame%d x%lf\n", trackIdx, audiometer.endofStream[trackIdx], x);
	*totalTime = (double)(audiometer.endofStream[trackIdx]*x);
	fprintf(file, "         \"pts\" : %lf,\n", (double)(audio_frame_count[trackIdx]*x));
	fprintf(file, "         \"val\" : [");
	for(int i=0; i<chnum; i++){
		fprintf(file, "[%d, %d]", *(min+i), *(max+i));
		if(i<chnum-1){
			fprintf(file,",");
		}
	}
	fprintf(file,"]\n");
	fprintf(file, "      }");
}

void writeJsonFrameAudioPhaseMeters(FILE* file, int chnum, int* arraynum, AVFrame* frame, AUDIOPHASEMETERS* meters, int trackIdx, int sampleNum){
	double val = av_get_audio_frame_duration(audio_dec_ctx[trackIdx], frame->pkt_size) / (double)audio_dec_ctx[trackIdx]->time_base.den;
	int loopnum = (chnum+1)/2;

	fprintf(file, "      {\n");
	//fprintf(file, "         \"index\" : %d,\n", audio_frame_count[trackIdx]);
	fprintf(file, "         \"pts\" : %lf,\n", (double)(audio_frame_count[trackIdx]*val));
	//fprintf(file, "         \"val\" : [\n");
	fprintf(file, "         \"val\" : [\n");
	for(int i=0; i<loopnum; i++){
		int end = (arraynum[i]>sampleNum) ? arraynum[i]-sampleNum: 0;
		//printf("ch%d arrayNum%d sampleNum%d end%d\n", i, arraynum[i], sampleNum, end);
		//fprintf(file, "            \"ch%d\" : [", i);
		fprintf(file, "            [");
		//fprintf(file, "            ");
		//fprintf(file, "            ");
		for(int j=arraynum[i]-1; j>=end; j--){
			//if(audio_frame_count[trackIdx] == 892)
			//printf("arrayNum%d i%d j%d x%d y%d\n", arraynum[i], i, j, meters[i].x[j], meters[i].y[j]);
			fprintf(file, "[%d,%d]", meters[i].x[j], meters[i].y[j]);
			if(j!=end){
				fprintf(file, ",");
			}
		}
		fprintf(file, "]");
		if(i!=loopnum-1){
			fprintf(file,",\n");
		}else{
			fprintf(file,"\n");
		}
	}
	fprintf(file,"         ]\n");
	fprintf(file, "      }");
}

void writeJsonInfoAudioPhaseMeters(FILE* file, signed char min, signed char max){

	fprintf(file, "   ],\n");
	fprintf(file, "   \"info\" : {\n");

	fprintf(file,"      \"min\" : %d,\n", min);
	fprintf(file,"      \"max\" : %d\n", max);
	fprintf(file,"   }\n}");
}


void *AudioLevelMetersFunc(void* arg){
	AVFrame *frame;
	uint8_t *converted_input_samples;

	uint8_t *pBuf;
	long values[MAX_NUM_CHANNEL];
	double levelsDb[MAX_NUM_CHANNEL];
	int levelDbInt[MAX_NUM_CHANNEL];
	int minWavCh[MAX_NUM_AUDIOTRACK][MAX_NUM_WAVEFORMSTEP][MAX_NUM_CHANNEL]; //16 is  1024(# of samples in a frame) divide by 64(the smallest # of samples in a pixel)
	int maxWavCh[MAX_NUM_AUDIOTRACK][MAX_NUM_WAVEFORMSTEP][MAX_NUM_CHANNEL];
	signed char minValPhaseMeter;
	signed char maxValPhaseMeter;
	int ch_num;
	signed short y;
	signed char m;
	int bufferIdx;
	int trackIdx;
	int featureIdx;
	AUDIOPHASEMETERS audioPhase[(MAX_NUM_CHANNEL+1)/2];
	int sampleNumAudioPhase[MAX_NUM_PHASEMETERSTEP];
	int arrayNum[MAX_NUM_PHASEMETERSTEP];
	int**** minWav = NULL;  // [AUDIOTRACK][FILE][FRAMEINDEX][CHANNEL]
	int**** maxWav = NULL;  // [AUDIOTRACK][FILE][FRAMEINDEX][CHANNEL]
	double totalTime[MAX_NUM_WAVEFORMSTEP];
	int isOddChannel;
	int print = 0;
	
	for(int i=0; i<MAX_NUM_AUDIOTRACK; i++){
		audio_frame_count[i] = 0;
	}	

	sampleNumAudioPhase[0] = 32;
	sampleNumAudioPhase[1] = 16;
	sampleNumAudioPhase[2] = 8;
	minValPhaseMeter = 127;
	maxValPhaseMeter = -128;

	trackIdx = (*(THREADCONTEXT*)arg).audioTrackIdx;
	featureIdx = (*(THREADCONTEXT*)arg).featureIdx;

	int searchRange[MAX_NUM_WAVEFORMSTEP];
	int tmp =1;
	for(int i=0; i< MAX_NUM_WAVEFORMSTEP; i++){
		searchRange[i] = tmp;
		tmp *=2;
	}

	if(featureIdx && 0x01){
		writeJsonBegin(output_file[0][trackIdx]);
		//printf("There %s %d tracks in Audio. ", (audiometer.numOfAudioTrack==1)?"is":"are", audiometer.numOfAudioTrack);
		//printf("Making Audio Level Meters from file '%s' into '%s'\n", src_filename, output_filename[0][trackIdx]);
	}
	if(featureIdx && 0x02){
	
		minWav = (int****)malloc(sizeof(int***)*MAX_NUM_AUDIOTRACK);
		maxWav = (int****)malloc(sizeof(int***)*MAX_NUM_AUDIOTRACK);

		for(int i=0; i<MAX_NUM_AUDIOTRACK; i++){
			minWav[i] = (int***)malloc(sizeof(int**)*MAX_NUM_WAVEFORMSTEP);
			maxWav[i] = (int***)malloc(sizeof(int**)*MAX_NUM_WAVEFORMSTEP);
			for(int j=0; j<MAX_NUM_WAVEFORMSTEP; j++){
				minWav[i][j] = (int**)malloc(sizeof(int*)*16);
				maxWav[i][j] = (int**)malloc(sizeof(int*)*16);

				for(int k=0; k<16; k++){
					minWav[i][j][k] = (int*)malloc(sizeof(int)*MAX_NUM_CHANNEL);
					maxWav[i][j][k] = (int*)malloc(sizeof(int)*MAX_NUM_CHANNEL);
				}
			}
		}
		for(int i=0; i<MAX_NUM_WAVEFORMSTEP; i++){
			writeJsonBegin(audio_waveform[i][trackIdx]);
			//printf("There %s %d tracks in Audio. ", (audiometer.numOfAudioTrack==1)?"is":"are", audiometer.numOfAudioTrack);
			//printf("Making Audio Waveform from file '%s' into '%s'\n", src_filename, audiowaveform_filename[i][trackIdx]);
			for(int l=0; l<MAX_NUM_CHANNEL; l++){
				minWavCh[trackIdx][i][l] = 128;
				maxWavCh[trackIdx][i][l] = -128;
			}
		}
	}
	if(featureIdx && 0x04){
		for(int i=0; i<MAX_NUM_PHASEMETERSTEP; i++){
			writeJsonBegin(audiophase_file[i][trackIdx]);
		//	printf("There %s %d tracks in Audio. ", (audiometer.numOfAudioTrack==1)?"is":"are", audiometer.numOfAudioTrack);
		//	printf("Making Audio Phase Meters from file '%s' into '%s'\n", src_filename, audiophase_filename[i][trackIdx]);
		}
	}
	if(trackIdx != 0){
		sleep(2);
	}
	else{
		sleep(1);
	}

	while(1){
		bufferIdx = audio_frame_count[trackIdx]%BUFFER_SIZE;
		frame = audiometer.frame[trackIdx][bufferIdx];
		ch_num = frame->channels;
		isOddChannel = (ch_num %2 == 1) ? 1 : 0;
		//if(!print){
		//	printf("track%d frame%d ch_num%d isOddchannel%d saveDone%d\n", trackIdx, audio_frame_count[trackIdx], ch_num, isOddChannel, audiometer.saveDone[trackIdx][bufferIdx]);
		//	print = 1;
		//}
		if(!audiometer.saveDone[trackIdx][bufferIdx]){

			int tmp= audio_frame_count[trackIdx] % 16;

			for(int j=0; j<MAX_NUM_WAVEFORMSTEP; j++){
				if(tmp%searchRange[j] == 0){
					for(int k=0; k<16; k++){
						for(int l=0; l<MAX_NUM_CHANNEL; l++){
							minWav[trackIdx][j][k][l] = 128;
							maxWav[trackIdx][j][k][l] = -128;
						}
					}
				}
			}

			if(audiometer.endofStream[trackIdx] == audio_frame_count[trackIdx]){
				//printf("EOF UP%d\n", audiometer.endOfStream);
				if(featureIdx && 0x01){
					//output_file[0][trackIdx]--;
					fseek(output_file[0][trackIdx], -2L, SEEK_CUR);
					writeJsonEnd(output_file[0][trackIdx]);
				}
				if(featureIdx && 0x02){
					for(int i=0; i<MAX_NUM_WAVEFORMSTEP; i++){
						writeJsonInfoAudioWaveform(audio_waveform[i][trackIdx], ch_num, trackIdx, minWavCh[trackIdx][i],
								maxWavCh[trackIdx][i], totalTime[i], audiometer.frame[trackIdx][bufferIdx]);

					}
					for(int i=0; i<MAX_NUM_AUDIOTRACK; i++){
						for(int j=0; j<MAX_NUM_WAVEFORMSTEP; j++){
							for(int k=0; k<16; k++){
								free(minWav[i][j][k]);
								free(maxWav[i][j][k]);
							}
							free(minWav[i][j]);
							free(maxWav[i][j]);

						}
						free(minWav[i]);
						free(maxWav[i]);
					}
					free(minWav);
					free(maxWav);
				}
				if(featureIdx && 0x04){
					for(int i=0; i<MAX_NUM_PHASEMETERSTEP; i++){
						//audiophase_file[i][trackIdx]--;
						fseek(audiophase_file[i][trackIdx], -2L, SEEK_CUR);
						writeJsonInfoAudioPhaseMeters(audiophase_file[i][trackIdx], minValPhaseMeter, maxValPhaseMeter);
					}
				}
				break;
			}
			usleep(200);
			continue;
		}
		audiometer.saveDone[trackIdx][bufferIdx] = 0;
		print = 0;
		for(int i=0; i< MAX_NUM_CHANNEL; i++){
			values[i] = 0.0;
			levelsDb[i] = -96.0;
			levelDbInt[i] = 0;
		}
		frame = audiometer.frame[trackIdx][bufferIdx];
		ch_num = frame->channels;
		converted_input_samples = audiometer.input_samples[trackIdx][bufferIdx];
		pBuf = converted_input_samples;

		for(int x=0; (x<frame->nb_samples); x++){
			for(int i=0; i< ch_num; i++){
				y =*((uint8_t*)pBuf+2);
				m = *((uint8_t*)pBuf+3);
				y |= m << 8;
				y = abs(y);

				if(y > values[i]){
					values[i] = y;
				}

				for(int z=0; z<MAX_NUM_WAVEFORMSTEP; z++){
					int tmp= audio_frame_count[trackIdx] % 16;

					if(m < minWav[trackIdx][z][tmp/searchRange[z]][i]){
						minWav[trackIdx][z][tmp/searchRange[z]][i] = m;
					}
					if(m > maxWav[trackIdx][z][tmp/searchRange[z]][i]){
						maxWav[trackIdx][z][tmp/searchRange[z]][i] = m;
					}
					
					if(m < minWavCh[trackIdx][z][i]){
						minWavCh[trackIdx][z][i] = m;
					}
					if(m > maxWavCh[trackIdx][z][i]){
						maxWavCh[trackIdx][z][i] = m;
					}

				}
				if(isOddChannel){
					if (i %2 == 0){
						audioPhase[i/2].y[x] = m;
						audioPhase[i/2].x[x] = 0;
					}
				}
				else{
					if(i%2==0){
						audioPhase[i/2].y[x] = m;
					}else{
						audioPhase[i/2].x[x] = m;
					}
				}

				if(isOddChannel){
					if (i %2 == 0){
						//printf("===TrackIdx%d x%d ch%d x%d y%d\n", trackIdx, x, i, audioPhase[i/2].x[x], audioPhase[i/2].y[x]);
						RatateAudioPhase(45, &audioPhase[i/2].x[x], &audioPhase[i/2].y[x]);
						audioPhase[i/2].abs[x]=audioPhase[i/2].x[x] * audioPhase[i/2].x[x] + audioPhase[i/2].y[x] * audioPhase[i/2].y[x];
						//printf("======TrackIdx%d x%d ch%d x%d y%d\n", trackIdx, x, i, audioPhase[i/2].x[x], audioPhase[i/2].y[x]);
					}
				}
				else{
					if(i%2==1){
						RatateAudioPhase(45, &audioPhase[i/2].x[x], &audioPhase[i/2].y[x]);
						audioPhase[i/2].abs[x]=audioPhase[i/2].x[x] * audioPhase[i/2].x[x] + audioPhase[i/2].y[x] * audioPhase[i/2].y[x];
					}
				}
				pBuf += 4;
			}
		}


		for(int i=0; i<ch_num; i++){
			if(isOddChannel){
				if(i%2==0){
					//printf("TrackIdx%d ch%d  x%d y%d\n", trackIdx, i, audioPhase[i/2].x[0], audioPhase[i/2].y[0]);
					quickSort(audioPhase[i/2].abs, audioPhase[i/2].x, audioPhase[i/2].y, 0, frame->nb_samples-1);
					arrayNum[i/2] = removeDuplicates(audioPhase[i/2].x,audioPhase[i/2].y, audioPhase[i/2].abs, sizeof(audioPhase[i/2].x));
				}
			}
			else{
				if(i%2==1){
					quickSort(audioPhase[i/2].abs, audioPhase[i/2].x, audioPhase[i/2].y, 0, frame->nb_samples-1);
					arrayNum[i/2] = removeDuplicates(audioPhase[i/2].x,audioPhase[i/2].y, audioPhase[i/2].abs, sizeof(audioPhase[i/2].x));
				}
			}

			double tmp = (values[i]+1)/32768.0;
			if(tmp != 0)
				levelsDb[i] = 20.0*log10(tmp);
			else{
				levelsDb[i] = -96;
			}

			if(levelsDb[i]> 0.0){
				levelsDb[i] = 0.0;
			}
			levelDbInt[i] = (int)levelsDb[i];
		}
#if LOG_FRAMEINFO
		printf("audio track: %d AudioLevelMeters n:%d nb_samples:%d pts:%s ", trackIdx,
				audio_frame_count[trackIdx], frame->nb_samples,
				av_ts2timestr(frame->pts, &audio_dec_ctx[trackIdx]->time_base));
		for(int i=0; i<ch_num; i++){
			printf("%f ", levelsDb[i]);
		}
		printf("\n");
#endif


#if FILEDUMP
		fwrite(converted_input_samples, 1, ch_num * framesize, audio_dst_file[trackIdx]);
#endif
		if(featureIdx && 0x01){
			writeJsonFrameAudioLevelMeters(output_file[0][trackIdx], ch_num, frame, levelDbInt, trackIdx);
		}
		if(featureIdx && 0x02){
			for(int z=0; z<MAX_NUM_WAVEFORMSTEP; z++){
				int tmp= audio_frame_count[trackIdx] % 16;
				if((audio_frame_count[trackIdx]+1)%searchRange[z] == 0){
					writeJsonFrameAudioWaveform(audio_waveform[z][trackIdx], ch_num, frame, minWav[trackIdx][z][tmp/searchRange[z]],
							maxWav[trackIdx][z][tmp/searchRange[z]], trackIdx, z, searchRange[z], &totalTime[z]);
				}
			}
		}
		if(featureIdx && 0x04){
			for(int j=0; j<(ch_num+1)/2;j++){
				for(int k=0; k< arrayNum[j];k++ ){
					if(audioPhase[j].x[k] < minValPhaseMeter){
						minValPhaseMeter = audioPhase[j].x[k];
					}
					if(audioPhase[j].y[k] < minValPhaseMeter){
						minValPhaseMeter = audioPhase[j].y[k];
					}

					if(audioPhase[j].x[k] > maxValPhaseMeter){
						maxValPhaseMeter = audioPhase[j].x[k];
					}
					if(audioPhase[j].y[k] > maxValPhaseMeter){
						maxValPhaseMeter = audioPhase[j].y[k];
					}

				}
			}
			for(int i=0; i<MAX_NUM_PHASEMETERSTEP; i++){
				writeJsonFrameAudioPhaseMeters(audiophase_file[i][trackIdx], ch_num, arrayNum, frame, audioPhase, trackIdx, sampleNumAudioPhase[i]);
			}
		}
		audiometer.processDone[trackIdx][bufferIdx] = 1;

		if(audiometer.endofStream[trackIdx] == audio_frame_count[trackIdx]){
			//printf("EOF DOWN%d\n", audiometer.endOfStream);
			if(featureIdx && 0x01){
				writeJsonEnd(output_file[0][trackIdx]);
			}
			if(featureIdx && 0x02){
				for(int i=0; i<MAX_NUM_WAVEFORMSTEP; i++){
					writeJsonInfoAudioWaveform(audio_waveform[i][trackIdx], ch_num, trackIdx, minWavCh[trackIdx][i],
							maxWavCh[trackIdx][i], totalTime[i], audiometer.frame[trackIdx][bufferIdx]);

				}
				for(int i=0; i<MAX_NUM_AUDIOTRACK; i++){
					for(int j=0; j<MAX_NUM_WAVEFORMSTEP; j++){
						for(int k=0; k<16; k++){
							free(minWav[i][j][k]);
							free(maxWav[i][j][k]);
						}
						free(minWav[i][j]);
						free(maxWav[i][j]);

					}
					free(minWav[i]);
					free(maxWav[i]);
				}
				free(minWav);
				free(maxWav);

			}
			if(featureIdx && 0x04){
				for(int i=0; i<MAX_NUM_PHASEMETERSTEP; i++){

					writeJsonInfoAudioPhaseMeters(audiophase_file[i][trackIdx], minValPhaseMeter, maxValPhaseMeter);
				}
			}
			break;
		}


		if(audiometer.endofStream[trackIdx] != audio_frame_count[trackIdx]){
			if(featureIdx && 0x01){
				writeJsonBetweenFrame(output_file[0][trackIdx]);
			}

			if(featureIdx && 0x04){
				for(int i=0; i<MAX_NUM_PHASEMETERSTEP; i++){
					writeJsonBetweenFrame(audiophase_file[i][trackIdx]);
				}
			}
		}
		//			writeJsonBetweenFrame(audio_waveform[0][trackIdx]);
		audio_frame_count[trackIdx]++;

	}
}


