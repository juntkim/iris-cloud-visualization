/*
 * global.h
 *
 *  Created on: Nov 29, 2018
 *      Author: june
 */

#ifndef GLOBAL_H_
#define GLOBAL_H_
#include "define.h"

AVFormatContext *fmt_ctx;
AVCodecContext *video_dec_ctx[MAX_NUM_VIDEOTRACK];
AVCodecContext *audio_dec_ctx[MAX_NUM_AUDIOTRACK];
int width, height;
enum AVPixelFormat pix_fmt;
AVStream *video_stream[MAX_NUM_VIDEOTRACK];
AVStream *audio_stream[MAX_NUM_AUDIOTRACK];
const char *src_filename;
const char *video_dst_filename;
const char *audio_dst_filename;
const char *audiolevelmeters_filename;
const char *base_filename;
int feature_index;
char *video_dst_filename2[MAX_NUM_VIDEOTRACK];
char *audio_dst_filename2[MAX_NUM_VIDEOTRACK];
char *audiolevelmeters_filename2[MAX_NUM_VIDEOTRACK];
char *output_filename[MAX_NUM_VISUALIZATION][MAX_NUM_TRACK];
char *audiowaveform_filename[MAX_NUM_WAVEFORMSTEP][MAX_NUM_TRACK];
char *audiophase_filename[MAX_NUM_PHASEMETERSTEP][MAX_NUM_TRACK];
char *filename_edited;
const char *progress_outfilename;

FILE *video_dst_file[MAX_NUM_VIDEOTRACK];
FILE *audio_dst_file[MAX_NUM_AUDIOTRACK];
FILE *audiolevelmeters_file[MAX_NUM_AUDIOTRACK];
FILE *output_file[MAX_NUM_VISUALIZATION][MAX_NUM_TRACK];
FILE *audio_waveform[MAX_NUM_WAVEFORMSTEP][MAX_NUM_TRACK];
FILE *audiophase_file[MAX_NUM_PHASEMETERSTEP][MAX_NUM_TRACK];
FILE *progress_outfile;

uint8_t *video_dst_data[MAX_NUM_VIDEOTRACK][4];
int      video_dst_linesize[MAX_NUM_VIDEOTRACK][4];
int video_dst_bufsize[MAX_NUM_VIDEOTRACK];
int video_stream_idx[MAX_NUM_VIDEOTRACK];
int audio_stream_idx[MAX_NUM_AUDIOTRACK];
int video_frame_count;
int refcount;
int nbframes;
AUDIOLEVELMETERS audiometer;
THREADCONTEXT tcontext[MAX_NUM_AUDIOTRACK];
int audioFrameCount[MAX_NUM_AUDIOTRACK];
int videoFrameCount[MAX_NUM_AUDIOTRACK];
int chMin[MAX_NUM_AUDIOTRACK][MAX_NUM_WAVEFORMSTEP][MAX_NUM_CHANNEL];
int chMax[MAX_NUM_AUDIOTRACK][MAX_NUM_WAVEFORMSTEP][MAX_NUM_CHANNEL];

#endif /* GLOBAL_H_ */
