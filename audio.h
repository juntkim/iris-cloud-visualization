/*
 * audiolevelmeters.h
 *
 *  Created on: Nov 21, 2018
 *      Author: june
 */

#ifndef AUDIO_H_
#define AUDIO_H_
#include "define.h"
#include <libavutil/imgutils.h>
#include <libavutil/samplefmt.h>
#include <libavutil/timestamp.h>
#include <libavformat/avformat.h>
#include <libavformat/avio.h>
#include <libswresample/swresample.h>

typedef struct AUDIOLEVELMETERS {
	uint8_t *(input_samples[MAX_NUM_AUDIOTRACK][BUFFER_SIZE]); //resampled decoded audio samples
	AVFrame *(frame[MAX_NUM_AUDIOTRACK][BUFFER_SIZE]);
	int saveDone[MAX_NUM_AUDIOTRACK][BUFFER_SIZE];
	int processDone[MAX_NUM_AUDIOTRACK][BUFFER_SIZE];
	AVCodecContext *video_dec_ctx[MAX_NUM_VIDEOTRACK];
	AVCodecContext *audio_dec_ctx[MAX_NUM_AUDIOTRACK];
	int numOfAudioTrack;
	int numOfVideoTrack;
	int audioTrackIdx[MAX_NUM_AUDIOTRACK];
	int videoTrackIdx[MAX_NUM_VIDEOTRACK];
	int endofStream[MAX_NUM_AUDIOTRACK];
	//int videoEOS[MAX_NUM_VIDEOTRACK];
} AUDIOLEVELMETERS;

typedef struct AUDIOPHASEMETERS {
	char x[2000];
	char y[2000];
	int abs[2000];

} AUDIOPHASEMETERS;

typedef struct THREADCONTEXT {
	int audioTrackIdx;
	int featureIdx;
} THREADCONTEXT;

int audio_frame_count[MAX_NUM_AUDIOTRACK];
void* AudioLevelMetersFunc(void* arg);

#endif /* AUDIO_H_ */
